import React from 'react';
import { render } from '@testing-library/react';
import App from './App';

test('renders header', () => {
  const { getByText } = render(<App />);
  const headerElement = getByText(/e1800957/i);
  expect(headerElement).toBeInTheDocument();
});
